CTEST_RESOURCE_SPEC_FILE
------------------------

.. versionadded:: 3.18

Specify the CTest ``ResourceSpecFile`` setting in a :manual:`ctest(1)`
dashboard client script.
